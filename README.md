# Doação de notas fiscais

Vamos neste projeto discutir idéias de projetos que devem facilitar o registro de notas fiscais para doação no site [https://www.nfp.fazenda.sp.gov.br](https://www.nfp.fazenda.sp.gov.br)

**Que tipo de ferramentas e processos podemos desenvolver para facilitar este trabalho?**

Algumas idéias:

* Aplicativo leitor de notas fiscais 
* Serviço que armazena-ra as imagens das notas e suas informações identificadas por [OCR](https://pt.wikipedia.org/wiki/Reconhecimento_%C3%B3tico_de_caracteres)
* plugin no navegador que irá exibir uma nota fiscal por vez e já inserir suas informações no formulário do site.

Podemos conversar e votar nas melhores idéias a partir de [issues](http://gitlab.com/doacaoNotasFiscais/doc/issues)

